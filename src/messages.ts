import { MESSAGES } from '@avstantso/node-or-browser-js--model-core';

const noAPIError = 'No API error';

export default { ...MESSAGES, noAPIError };

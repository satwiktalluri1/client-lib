import { Cases } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';

export function shortModelName<TModel extends Model.Structure>(
  model: Model.Declaration<TModel>
): string {
  return Cases.camel(model.name.split('.').pop());
}

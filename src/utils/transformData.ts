import { JS } from '@avstantso/node-or-browser-js--utils';
import { DataDecorator } from '@avstantso/node-or-browser-js--model-core';

export function TransformData<T = any>(decorator: DataDecorator<any>) {
  const transformData = (data: any): T => {
    if (!data || !JS.is.object(data)) return data;

    const r = decorator(data);
    Object.keys(r).forEach((key) => {
      const value = r[key as keyof object];

      if (Array.isArray(value))
        return (r[key as keyof object] = value.map(transformData));

      if (!(value instanceof Date) && JS.is.object(value))
        return (r[key as keyof object] = transformData(value));
    });

    return r;
  };

  return transformData;
}

import { validate as isValidUUID } from 'uuid';
import { Model, Data } from '@avstantso/node-or-browser-js--model-core';

import { APIExec, API } from '@types';
import MESSAGES from '@messages';
import { SocketLib } from '@socketLib';
import { cutAndValidate, shortModelName } from '@utils';

import { ExecutorFactory, ExecDataOpts, isExecutor } from './api-exec';

export namespace AbstractApi {
  export interface Overload {
    <
      TModel extends Model.Structure,
      TSocketLib extends SocketLib.Abstract.Structure
    >(
      model: Model.Declaration<TModel>,
      executor: APIExec.Executor<TModel, TSocketLib>
    ): API.Abstract.Factory<TModel, TSocketLib>;

    <
      TModel extends Model.Structure,
      TSocketLib extends SocketLib.Abstract.Structure
    >(
      model: Model.Declaration<TModel>,
      apiOptions: TSocketLib['Options'] &
        APIExec.Executor.Options &
        APIExec.Executor.Options.SocketLib<TSocketLib>
    ): API.Abstract.Factory<TModel, TSocketLib>;
  }
}

export const AbstractApi: AbstractApi.Overload = <
  TModel extends Model.Structure,
  TSocketLib extends SocketLib.Abstract.Structure
>(
  model: Model.Declaration<TModel>,
  param: unknown
): API.Abstract.Factory<TModel, TSocketLib> => {
  const Executor = ExecutorFactory<TSocketLib>();

  const executor: APIExec.Executor<TModel, TSocketLib> = isExecutor<
    TModel,
    TSocketLib
  >(param)
    ? param
    : Executor(
        model,
        param as TSocketLib['Options'] &
          APIExec.Executor.Options &
          APIExec.Executor.Options.SocketLib<TSocketLib>
      );

  const { getUrl, exec, clientChecks } = executor;

  const list: API.Abstract.Readable.List<TModel, TSocketLib> = (
    ...params: any[]
  ): Promise<TModel['Select'][]> =>
    exec<TModel['Select'][]>(
      'list',
      () => {
        const isOptions = Model.Is.Select.Options;
        const isConfig = (param: any): boolean => param && !isOptions(param);

        const options: Model.Select.Options<TModel> =
          (params.length > 0 && isOptions<TModel>(params[0]) && params[0]) ||
          (params.length > 1 && isOptions<TModel>(params[1]) && params[1]) ||
          undefined;
        const config: TSocketLib['Config'] =
          (params.length > 0 && isConfig(params[0]) && params[0]) ||
          (params.length > 1 && isConfig(params[1]) && params[1]) ||
          undefined;

        // TODO
        if (options) throw Error(`TODO transfer options for "list"`);

        return config;
      },
      ({ socketLib, url, config }) =>
        socketLib.get<Data<TModel['Select'][]>>(url, config),
      { defaultValue: [], ...ExecDataOpts }
    );

  const one: API.Abstract.Readable.One<TModel, TSocketLib> = (id, config?) =>
    exec<TModel['Select']>(
      'one',
      ({ paramsError }) => {
        if (clientChecks) {
          if (!id) throw paramsError(MESSAGES.idNotSet);

          if (!isValidUUID(id))
            throw paramsError(`${MESSAGES.invalidUUIDFormatForId} "${id}"`);
        }

        return getUrl(shortModelName(model), id);
      },
      ({ socketLib, url }) =>
        socketLib.get<Data<TModel['Select']>>(url, config),
      ExecDataOpts
    );

  const insert: API.Abstract.Writable.Insert<TModel, TSocketLib> = (
    entity,
    config?
  ) =>
    exec<TModel['Select'], TModel['Insert']>(
      'insert',
      (erc) => {
        if (clientChecks) {
          if (!entity) throw erc.paramsError(MESSAGES.dataNotSet);
        }

        const data = cutAndValidate<
          TModel['Insert'] | TModel['Select'],
          TModel['Insert']
        >(entity, model.Schema.Insert, erc, clientChecks);

        return [config, data];
      },
      ({ socketLib, url, data }) =>
        socketLib.put<Data<TModel['Select']>, Data<TModel['Insert']>>(
          url,
          { data },
          config
        ),
      ExecDataOpts
    );

  const update: API.Abstract.Writable.Update<TModel, TSocketLib> = (
    entity,
    config?
  ) =>
    exec<TModel['Select'], TModel['Update']>(
      'update',
      (erc) => {
        if (clientChecks) {
          if (!entity) throw erc.paramsError(MESSAGES.dataNotSet);
        }

        const data = cutAndValidate<
          TModel['Update'] | TModel['Select'],
          TModel['Update']
        >(entity, model.Schema.Update, erc, clientChecks);

        return [config, data];
      },
      ({ socketLib, url, data }) =>
        socketLib.post<Data<TModel['Select']>, Data<TModel['Update']>>(
          url,
          { data },
          config
        ),
      ExecDataOpts
    );

  const _delete: API.Abstract.Writable.Delete<TSocketLib> = (id, config?) =>
    exec<Model.ID>(
      'delete',
      ({ paramsError }) => {
        if (clientChecks) {
          if (!id) throw paramsError(MESSAGES.idNotSet);

          if (!isValidUUID(id))
            throw paramsError(`${MESSAGES.invalidUUIDFormatForId} "${id}"`);
        }

        return config;
      },
      ({ socketLib, url }) => socketLib.delete<Data<Model.ID>>(url, id, config),
      ExecDataOpts
    );

  const Readable = () => ({ list, one });
  const Writable = () => ({ insert, update, delete: _delete });
  const ReadableWritable = () => ({ ...Readable(), ...Writable() });

  return {
    Readable,
    Writable,
    ReadableWritable,
  };
};

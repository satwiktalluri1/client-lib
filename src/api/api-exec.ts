import Bluebird from 'bluebird';
import { JSONSchema7 } from 'json-schema';

import { Generics, JS } from '@avstantso/node-or-browser-js--utils';
import {
  Model,
  Data,
  DataDecorator,
  validateBySchema,
} from '@avstantso/node-or-browser-js--model-core';

import { APIExec } from '@types';
import MESSAGES from '@messages';
import { SocketLib } from '@socketLib';
import { TransformData, defaultDataDecorator, shortModelName } from '@utils';
import { ParamsError } from '@classes';

export const ExecDataOpts = {
  needTransform: true,
  getData: <T>(resData: Data<T>) => resData?.data,
};

export function isExecutor<
  TModel extends Model.Structure = Model.Structure,
  TSocketLib extends SocketLib.Abstract.Structure = SocketLib.Abstract.Structure
>(candidate: unknown): candidate is APIExec.Executor<TModel, TSocketLib> {
  return (
    null !== candidate &&
    JS.is.object(candidate) &&
    'getSocketLib' in candidate &&
    'transformData' in candidate &&
    'getUrl' in candidate &&
    'exec' in candidate
  );
}

export const ExecutorFactory: APIExec.Executor.Factory = <
  TSocketLib extends SocketLib.Abstract.Structure
>() => {
  const Executor: APIExec.Executor.Overload<TSocketLib> = <
    TModel extends Model.Structure
  >(
    model: Model.Declaration<TModel>,
    apiOptions: TSocketLib['Options'] &
      APIExec.Executor.Options &
      APIExec.Executor.Options.SocketLib<TSocketLib>
  ): APIExec.Executor<TModel, TSocketLib> => {
    const { clientChecks, GetSocketLib, getCatcher, getMasterUrl } = apiOptions;
    const getSocketLib = GetSocketLib(apiOptions);

    const transformData = TransformData(
      apiOptions.decorator || DataDecorator(defaultDataDecorator)
    );

    const getUrl = (relUrlPart1: string, ...relUrlParts: string[]) =>
      [
        ...(getMasterUrl ? [getMasterUrl()] : []),
        relUrlPart1,
        ...relUrlParts,
      ].join('/');

    const exec: APIExec.Exec<TSocketLib> = <
      TResult,
      TInData = never,
      TResponse = Data<TResult>
    >(
      method: string,
      prepare: APIExec.Exec.Prepare<TSocketLib, TInData>,
      query: APIExec.Exec.Query<TSocketLib, TInData, TResponse>,
      options?: APIExec.Exec.Options<TResult, TResponse>
    ) => {
      const { defaultValue, needTransform, getData } = options || {};

      function paramsError(message: string) {
        return new ParamsError(model.name, method, message);
      }

      function validate(data: TInData, schema: JSONSchema7) {
        const { isValid, errors } = validateBySchema(data, schema);
        if (isValid) return;

        const e = new ParamsError(
          model.name,
          method,
          MESSAGES.dataDoesNotMatchSchema
        );

        e.details = errors;

        throw e;
      }

      return Bluebird.resolve(
        prepare({
          model: shortModelName(model),
          method,
          paramsError,
          validateBySchema: validate,
        })
      ).then((params: any) => {
        const isParamsArr = Array.isArray(params);
        const hasUrl = JS.is.string(isParamsArr ? params[0] : params);

        const url: string = hasUrl
          ? isParamsArr
            ? params[0]
            : params
          : getUrl(shortModelName(model));
        const config: TSocketLib['Config'] = isParamsArr
          ? params[hasUrl ? 1 : 0]
          : params;
        const data: TInData = isParamsArr && params[hasUrl ? 2 : 1];

        const socketLib = getSocketLib(getCatcher(model.name, method));

        return Bluebird.resolve(query({ socketLib, url, config, data }))
          .then((res) => (getData ? getData(res) : Generics.Cast.To(res)))
          .then((data) =>
            undefined === defaultValue ? data : data || defaultValue
          )
          .then((data) =>
            !needTransform
              ? data
              : Array.isArray(data)
              ? data.map(transformData)
              : transformData(data)
          );
      });
    };

    return {
      getSocketLib,
      clientChecks,
      transformData,
      getUrl,
      exec,
    };
  };

  return Executor;
};

import type { Model } from '@avstantso/node-or-browser-js--model-core';

import type * as Types from '@types';
import { SocketLib } from '@socketLib';

import { ExecutorFactory, ExecDataOpts, isExecutor } from './api-exec';
import { AbstractApi } from './abstract';
import { AxiosApi } from './axios';

export namespace API {
  export namespace Executor {
    export type GetCatcher = Types.APIExec.GetCatcher;
    export type Options = Types.APIExec.Executor.Options;

    export namespace Options {
      export type SocketLib<TSocketLib extends SocketLib.Abstract.Structure> =
        Types.APIExec.Executor.Options.SocketLib<TSocketLib>;
    }

    export namespace Abstract {
      export type Exec<TSocketLib extends SocketLib.Abstract.Structure> =
        Types.APIExec.Exec<TSocketLib>;
    }

    export type Abstract<
      TModel extends Model.Structure,
      TSocketLib extends SocketLib.Abstract.Structure
    > = Types.APIExec.Executor<TModel, TSocketLib>;

    export namespace Axios {
      export type Exec = Types.APIExec.Exec<SocketLib.Axios.Structure>;
    }

    export type Axios<TModel extends Model.Structure> = Types.APIExec.Executor<
      TModel,
      SocketLib.Axios.Structure
    >;
  }

  export namespace Abstract {
    export type Options<TSocketLib extends SocketLib.Abstract.Structure> =
      TSocketLib['Options'] &
        Executor.Options &
        Executor.Options.SocketLib<TSocketLib>;

    export namespace Readable {
      export type List<
        TModel extends Model.Structure,
        TSocketLib extends SocketLib.Abstract.Structure
      > = Types.API.Abstract.Readable.List<TModel, TSocketLib>;

      export type One<
        TModel extends Model.Structure,
        TSocketLib extends SocketLib.Abstract.Structure
      > = Types.API.Abstract.Readable.One<TModel, TSocketLib>;
    }

    export type Readable<
      TModel extends Model.Structure,
      TSocketLib extends SocketLib.Abstract.Structure
    > = Types.API.Abstract.Readable<TModel, TSocketLib>;

    export namespace Writable {
      export type Insert<
        TModel extends Model.Structure,
        TSocketLib extends SocketLib.Abstract.Structure
      > = Types.API.Abstract.Writable.Insert<TModel, TSocketLib>;

      export type Update<
        TModel extends Model.Structure,
        TSocketLib extends SocketLib.Abstract.Structure
      > = Types.API.Abstract.Writable.Update<TModel, TSocketLib>;

      export type Delete<TSocketLib extends SocketLib.Abstract.Structure> =
        Types.API.Abstract.Writable.Delete<TSocketLib>;
    }

    export type Writable<
      TModel extends Model.Structure,
      TSocketLib extends SocketLib.Abstract.Structure
    > = Types.API.Abstract.Writable<TModel, TSocketLib>;

    export type ReadableWritable<
      TModel extends Model.Structure,
      TSocketLib extends SocketLib.Abstract.Structure
    > = Types.API.Abstract.ReadableWritable<TModel, TSocketLib>;

    export type Factory<
      TModel extends Model.Structure,
      TSocketLib extends SocketLib.Abstract.Structure
    > = Types.API.Abstract.Factory<TModel, TSocketLib>;

    export type Union<
      TModel extends Model.Structure,
      TSocketLib extends SocketLib.Abstract.Structure
    > = Readable<TModel, TSocketLib> | ReadableWritable<TModel, TSocketLib>;
  }

  export namespace Axios {
    export type Options = SocketLib.Axios.Structure['Options'] &
      Executor.Options;

    export namespace Readable {
      export type List<TModel extends Model.Structure> =
        Types.API.Axios.Readable.List<TModel>;

      export type One<TModel extends Model.Structure> =
        Types.API.Axios.Readable.One<TModel>;
    }

    export type Readable<TModel extends Model.Structure> =
      Types.API.Axios.Readable<TModel>;

    export namespace Writable {
      export type Insert<TModel extends Model.Structure> =
        Types.API.Axios.Writable.Insert<TModel>;

      export type Update<TModel extends Model.Structure> =
        Types.API.Axios.Writable.Update<TModel>;

      export type Delete = Types.API.Axios.Writable.Delete;
    }

    export type Writable<TModel extends Model.Structure> =
      Types.API.Axios.Writable<TModel>;

    export type ReadableWritable<TModel extends Model.Structure> =
      Types.API.Axios.ReadableWritable<TModel>;

    export type Factory<TModel extends Model.Structure> =
      Types.API.Axios.Factory<TModel>;

    export type Union<TModel extends Model.Structure> =
      | Readable<TModel>
      | ReadableWritable<TModel>;
  }
}

export const API = {
  Executor: {
    DataOpts: ExecDataOpts,
    is: isExecutor,

    Abstract: { Factory: ExecutorFactory },
    Axios: ExecutorFactory<SocketLib.Axios.Structure>(),
  },
  Abstract: AbstractApi,
  Axios: AxiosApi,
};

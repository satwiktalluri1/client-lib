import { AxiosRequestConfig, AxiosResponse } from 'axios';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import { SocketLib } from '@types';

export const GetAxiosSocketLib: SocketLib.Axios.Get = (options) => {
  const { axiosInstance } = options;

  return (catcher) => {
    const R = <TOut>(
      promise: Promise<AxiosResponse<TOut, any>>
    ): Promise<TOut> => promise.catch(catcher).then((res) => res.data);

    const get = <TOut>(
      url: string,
      config?: AxiosRequestConfig
    ): Promise<TOut> => R(axiosInstance.get<TOut>(url, config));

    const put = <TOut, TIn>(
      url: string,
      data: TIn,
      config?: AxiosRequestConfig
    ): Promise<TOut> => R(axiosInstance.put<TOut>(url, data, config));

    const post = <TOut, TIn>(
      url: string,
      data: TIn,
      config?: AxiosRequestConfig
    ): Promise<TOut> => R(axiosInstance.post<TOut>(url, data, config));

    const _delete = <TOut>(
      url: string,
      id: Model.ID,
      config?: AxiosRequestConfig
    ): Promise<TOut> =>
      R(axiosInstance.delete<TOut>(url, { ...(config || {}), data: { id } }));

    return {
      get,
      put,
      post,
      delete: _delete,
    };
  };
};

import type { Model } from '@avstantso/node-or-browser-js--model-core';

export interface Structure {
  Options: unknown;
  Config: unknown;
  Error: Error;
}

export interface SocketLib<TStructure extends Structure> {
  get<TOut>(url: string, config?: TStructure['Config']): Promise<TOut>;
  put<TOut, TIn = any>(
    url: string,
    data: TIn,
    config?: TStructure['Config']
  ): Promise<TOut>;
  post<TOut, TIn = any>(
    url: string,
    data: TIn,
    config?: TStructure['Config']
  ): Promise<TOut>;
  delete<TOut>(
    url: string,
    id: Model.ID,
    config?: TStructure['Config']
  ): Promise<TOut>;
}

export type GetInternal<TStructure extends Structure> = (
  catcher: (e: Error) => Promise<never>
) => SocketLib<TStructure>;

export type Get<TStructure extends Structure> = (
  options: TStructure['Options']
) => GetInternal<TStructure>;

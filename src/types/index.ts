import type * as SocketLib from './socketLib';
import type * as APIExec from './api-exec';
import type * as API from './api';

export type { Authorized } from './authorized';
export type { SocketLib, APIExec, API };

export interface Authorized {
  jwt: Readonly<string>;
}

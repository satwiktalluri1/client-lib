import type { JSONSchema7 } from 'json-schema';
import type {
  Model,
  Data,
  DataDecorator,
} from '@avstantso/node-or-browser-js--model-core';
import { ParamsError } from '@classes';
import type * as SocketLib from '../socketLib';

export type Catcher = (e: Error) => Promise<never>;
export type GetCatcher = (modelUrl: string, method: string) => Catcher;

export namespace Exec {
  export interface Options<TResult, TResponse> {
    defaultValue?: TResult;
    needTransform?: boolean;
    getData?(res: TResponse): TResult;
  }

  export namespace Prepare {
    export interface Context<TInData> {
      model: Readonly<string>;
      method: Readonly<string>;
      paramsError(message: string): ParamsError;
      validateBySchema(data: TInData, schema: JSONSchema7): void | never;
    }

    export namespace Result {
      export type Item<
        TSocketLib extends SocketLib.Abstract.Structure,
        TInData
      > =
        | string
        | TSocketLib['Config']
        | [string, TSocketLib['Config']?, TInData?]
        | [TSocketLib['Config'], TInData?];
    }

    export type Result<
      TSocketLib extends SocketLib.Abstract.Structure,
      TInData
    > =
      | Prepare.Result.Item<TSocketLib, TInData>
      | Promise<Prepare.Result.Item<TSocketLib, TInData>>;
  }

  export type Prepare<
    TSocketLib extends SocketLib.Abstract.Structure,
    TInData
  > = (
    context: Prepare.Context<TInData>
  ) => Prepare.Result<TSocketLib, TInData>;

  export namespace Query {
    export interface Context<
      TSocketLib extends SocketLib.Abstract.Structure,
      TInData
    > {
      socketLib: SocketLib.Abstract.SocketLib<TSocketLib>;
      url?: string;
      config?: TSocketLib['Config'];
      data?: TInData;
    }
  }

  export type Query<
    TSocketLib extends SocketLib.Abstract.Structure,
    TInData,
    TResponse
  > = (context: Query.Context<TSocketLib, TInData>) => Promise<TResponse>;
}

export type Exec<TSocketLib extends SocketLib.Abstract.Structure> = <
  TResult = void,
  TInData = never,
  TResponse = Data<TResult>
>(
  method: string,
  prepare: Exec.Prepare<TSocketLib, TInData>,
  query: Exec.Query<TSocketLib, TInData, TResponse>,
  options?: Exec.Options<TResult, TResponse>
) => Promise<TResult>;

export namespace Executor {
  export namespace Options {
    export interface SocketLib<
      TSocketLib extends SocketLib.Abstract.Structure
    > {
      GetSocketLib: SocketLib.Abstract.Get<TSocketLib>;
      getCatcher: GetCatcher;
    }
  }

  export interface Options {
    decorator?: DataDecorator;
    getMasterUrl?: () => string;
    clientChecks?: boolean;
  }

  export type Overload<TSocketLib extends SocketLib.Abstract.Structure> = {
    <TModel extends Model.Structure>(
      model: Model.Declaration<TModel>,
      apiOptions: TSocketLib['Options'] &
        Options &
        Options.SocketLib<TSocketLib>
    ): Executor<TModel, TSocketLib>;
  };

  export type Factory = <
    TSocketLib extends SocketLib.Abstract.Structure
  >() => Overload<TSocketLib>;
}

export interface Executor<
  TModel extends Model.Structure,
  TSocketLib extends SocketLib.Abstract.Structure
> {
  getSocketLib: SocketLib.Abstract.GetInternal<TSocketLib>;
  clientChecks: Readonly<boolean>;
  transformData(data: any): TModel['Select'];
  getUrl(relUrlPart1: string, ...relUrlParts: string[]): string;
  exec: Exec<TSocketLib>;
}

import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type * as SocketLib from '../socketLib';

export namespace Readable {
  export type List<
    TModel extends Model.Structure,
    TSocketLib extends SocketLib.Abstract.Structure
  > = {
    (
      options?: Model.Select.Options<TModel>,
      config?: TSocketLib['Config']
    ): Promise<TModel['Select'][]>;
    (config: TSocketLib['Config']): Promise<TModel['Select'][]>;
  };

  export type One<
    TModel extends Model.Structure,
    TSocketLib extends SocketLib.Abstract.Structure
  > = (
    id: Model.ID,
    config?: TSocketLib['Config']
  ) => Promise<TModel['Select']>;
}

export interface Readable<
  TModel extends Model.Structure,
  TSocketLib extends SocketLib.Abstract.Structure
> {
  list: Readable.List<TModel, TSocketLib>;
  one: Readable.One<TModel, TSocketLib>;
}

export namespace Writable {
  export type Insert<
    TModel extends Model.Structure,
    TSocketLib extends SocketLib.Abstract.Structure
  > = (
    entity: TModel['Insert'] | TModel['Select'],
    config?: TSocketLib['Config']
  ) => Promise<TModel['Select']>;

  export type Update<
    TModel extends Model.Structure,
    TSocketLib extends SocketLib.Abstract.Structure
  > = (
    entity: TModel['Update'] | TModel['Select'],
    config?: TSocketLib['Config']
  ) => Promise<TModel['Select']>;

  export type Delete<TSocketLib extends SocketLib.Abstract.Structure> = (
    id: Model.ID,
    config?: TSocketLib['Config']
  ) => Promise<Model.ID>;
}

export interface Writable<
  TModel extends Model.Structure,
  TSocketLib extends SocketLib.Abstract.Structure
> {
  insert: Writable.Insert<TModel, TSocketLib>;
  update: Writable.Update<TModel, TSocketLib>;
  delete: Writable.Delete<TSocketLib>;
}

export type ReadableWritable<
  TModel extends Model.Structure,
  TSocketLib extends SocketLib.Abstract.Structure
> = Readable<TModel, TSocketLib> & Writable<TModel, TSocketLib>;

export interface Factory<
  TModel extends Model.Structure,
  TSocketLib extends SocketLib.Abstract.Structure
> {
  Readable(): Readable<TModel, TSocketLib>;
  Writable(): Writable<TModel, TSocketLib>;
  ReadableWritable(): ReadableWritable<TModel, TSocketLib>;
}

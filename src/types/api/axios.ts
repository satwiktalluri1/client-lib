import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type * as SocketLib from '../socketLib';
import type * as Abstract from './abstract';

export namespace Readable {
  export type List<TModel extends Model.Structure> = Abstract.Readable.List<
    TModel,
    SocketLib.Axios.Structure
  >;

  export type One<TModel extends Model.Structure> = Abstract.Readable.One<
    TModel,
    SocketLib.Axios.Structure
  >;
}

export type Readable<TModel extends Model.Structure> = Abstract.Readable<
  TModel,
  SocketLib.Axios.Structure
>;

export namespace Writable {
  export type Insert<TModel extends Model.Structure> = Abstract.Writable.Insert<
    TModel,
    SocketLib.Axios.Structure
  >;

  export type Update<TModel extends Model.Structure> = Abstract.Writable.Update<
    TModel,
    SocketLib.Axios.Structure
  >;

  export type Delete = Abstract.Writable.Delete<SocketLib.Axios.Structure>;
}

export type Writable<TModel extends Model.Structure> = Abstract.Writable<
  TModel,
  SocketLib.Axios.Structure
>;

export type ReadableWritable<TModel extends Model.Structure> =
  Abstract.ReadableWritable<TModel, SocketLib.Axios.Structure>;

export type Factory<TModel extends Model.Structure> = Abstract.Factory<
  TModel,
  SocketLib.Axios.Structure
>;

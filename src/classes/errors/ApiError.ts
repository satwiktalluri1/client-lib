import axios, { AxiosError } from 'axios';

import { Generics, JS } from '@avstantso/node-or-browser-js--utils';

import messages from '@messages';
import { MethodError } from './MethodError';

export type ErrorCallback<TError extends Error> = (e: TError) => Promise<never>;
export type AxiosErrorCallback = ErrorCallback<AxiosError>;

// errors from server
export class ApiError extends MethodError {
  statusCode: number;

  constructor(
    model: string,
    method: string,
    message: string,
    internal?: Error
  ) {
    super(model, method, message, internal);

    Object.setPrototypeOf(this, ApiError.prototype);
  }

  static is(error: unknown): error is ApiError {
    return JS.is.error(this, error);
  }

  static reporter(
    model: string,
    method: string
  ): MethodError.Reporter<ApiError> {
    return MethodError.makeReporter(
      model,
      method,
      (message: string, internal?: Error) =>
        new ApiError(model, method, message, internal)
    );
  }

  static fromAxios(model: string, method: string): AxiosErrorCallback {
    return (e: AxiosError): Promise<never> => {
      if (axios.isCancel(Generics.Cast(e))) throw e;

      const data = Generics.Cast(e?.response)?.data;
      const [message, statusCode, details] = !data
        ? [e.message, 0, null]
        : [data.message, e.response.status, data.details];

      const ae = new ApiError(model, method, message, e);

      ae.statusCode = statusCode;

      if (details) ae.details = details;

      throw ae;
    };
  }

  // for tests
  static fail(): never {
    throw Error(messages.noAPIError);
  }
}

import { JS } from '@avstantso/node-or-browser-js--utils';
import { MethodError } from './MethodError';

// local errors
export class ParamsError extends MethodError {
  constructor(
    model: string,
    method: string,
    message: string,
    internal?: Error
  ) {
    super(model, method, message, internal);

    Object.setPrototypeOf(this, ParamsError.prototype);
  }

  static is(error: unknown): error is ParamsError {
    return JS.is.error(this, error);
  }

  static reporter(
    model: string,
    method: string
  ): MethodError.Reporter<ParamsError> {
    return MethodError.makeReporter(
      model,
      method,
      (message: string, internal?: Error) =>
        new ParamsError(model, method, message, internal)
    );
  }
}

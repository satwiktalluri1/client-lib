import { JS } from '@avstantso/node-or-browser-js--utils';
import { ModelMethodError } from '@avstantso/node-or-browser-js--model-core';

export namespace MethodError {
  export interface Reporter<T extends MethodError> {
    (message: string, internal?: Error): T;

    model: Readonly<string>;
    method: Readonly<string>;
  }
}

export class MethodError extends ModelMethodError {
  constructor(
    model: string,
    method: string,
    message: string,
    internal?: Error
  ) {
    super(model, method, message, internal);

    Object.setPrototypeOf(this, MethodError.prototype);
  }

  static is(error: unknown): error is MethodError {
    return JS.is.error(this, error);
  }

  protected static makeReporter<T extends MethodError>(
    model: string,
    method: string,
    make: (message: string, internal?: Error) => T
  ): MethodError.Reporter<T> {
    const report = (message: string, internal?: Error) =>
      make(message, internal);

    report.model = model;
    report.method = method;

    return report;
  }
}

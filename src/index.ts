export type { Authorized } from './types';
export { MethodError, ParamsError, ApiError } from './classes';
export * from './socketLib';
export * from './utils';
export * from './api';
